/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolse.modelo.excepciones;

/**
 *
 * @author juand
 */
public class ArbolBinarioExcepciones extends Exception {
    
     public ArbolBinarioExcepciones() {
         
    }

    public ArbolBinarioExcepciones(String message) {
        super(message);
    }

    public ArbolBinarioExcepciones(String message, Throwable cause) {
        super(message, cause);
    }

    public ArbolBinarioExcepciones(Throwable cause) {
        super(cause);
    }
    
    
}



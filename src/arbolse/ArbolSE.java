/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbolse;

import arbolse.modelo.excepciones.ArbolBinarioExcepciones;
import javax.swing.JOptionPane;

/**
 *
 * @author juand
 */
public class ArbolSE {

    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        // TODO code application logic here
           arbolse.modelo.ArbolBinario arbol= new arbolse.modelo.ArbolBinario();
       
        try {
            arbol.adicionarNodo(4, arbol.getRaiz());
            
            arbol.adicionarNodo(-2, arbol.getRaiz());
            
            arbol.adicionarNodo(8, arbol.getRaiz());
          
            arbol.adicionarNodo(Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número")), 
                    arbol.getRaiz());
            
            
        } 
       catch(NumberFormatException n)
        {
             JOptionPane.showMessageDialog(null,"Debe ingresar un número");
        } 
        catch (ArbolBinarioExcepciones ex) {

            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
     }
}
    
    

                




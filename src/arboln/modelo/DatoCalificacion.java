/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboln.modelo;

import arboln.modelo.Visitante;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author juand
 */
public class DatoCalificacion implements Serializable {

    private Visitante visitante;
    private ArrayList<Calificacion> listaCalificaciones;

    public DatoCalificacion() {
    }

    public DatoCalificacion(Visitante visitante, ArrayList<Calificacion> listaCalificaciones) {
        this.visitante = visitante;
        this.listaCalificaciones = listaCalificaciones;
    }

    public Visitante getVisitante() {
        return visitante;
    }

    public void setVisitante(Visitante visitante) {
        this.visitante = visitante;
    }

    public ArrayList<Calificacion> getLstCalificaciones() {
        return listaCalificaciones;
    }

    public void setLstCalificaciones(ArrayList<Calificacion> lstCalificaciones) {
        this.listaCalificaciones = lstCalificaciones;
    }

    public void aumentarHijo(Calificacion calificacion) {
        listaCalificaciones.add(calificacion);
    }

    @Override
    public String toString() {
        return "Id: "+visitante.getIdentificacion();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboln.modelo;

/**
 *
 * @author juand
 */
public class CalificacionaAtraccion {
    private Atraccion atraccionC;
    private ArbolAVL encuesta;

    public CalificacionaAtraccion(Atraccion atraccionC, ArbolAVL encuesta) {
        this.atraccionC = atraccionC;
        this.encuesta = encuesta;
    }

    public Atraccion getAtraccionC() {
        return atraccionC;
    }

    public void setAtraccionC(Atraccion atraccionC) {
        this.atraccionC = atraccionC;
    }

    public ArbolAVL getEncuesta() {
        return encuesta;
    }

    public void setEncuesta(ArbolAVL encuesta) {
        this.encuesta = encuesta;
    }

    

    
    
}

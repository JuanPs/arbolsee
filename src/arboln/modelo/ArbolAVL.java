/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboln.modelo;


import arbolse.modelo.excepciones.ArbolBinarioExcepciones;
import arbolse.modelo.excepciones.ArbolNException;
import java.util.ArrayList;

/**
 *
 * @author juand
 */
public class ArbolAVL {
    
    
  public ArbolAVL() {
    }
    private NodoAVL raiz;
    private DatoCalificacion dato;

    public NodoAVL getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoAVL raiz) {
        this.raiz = raiz;
    }

    public DatoCalificacion getDato() {
        return dato;
    }

    public void setDato(DatoCalificacion dato) {
        this.dato = dato;
    }

    public void adicionarNodo(DatoCalificacion dato, NodoAVL ubicacion) throws ArbolNException {
        if (raiz == null) {
            raiz = new NodoAVL(dato);
        } else {
            if (dato.getVisitante().getIdentificacion().compareTo(ubicacion.getDato().getVisitante().getIdentificacion()) < 0) {
                if (ubicacion.getIzquierda() == null) {
                    ubicacion.setIzquierda(new NodoAVL(dato));

                } else {
                    adicionarNodo(dato, ubicacion.getIzquierda());
                }
            } else if (dato.getVisitante().getIdentificacion().compareTo(ubicacion.getDato().getVisitante().getIdentificacion()) > 0) {
                if (ubicacion.getDerecha() == null) {
                    ubicacion.setDerecha(new NodoAVL(dato));
                } else {
                    adicionarNodo(dato, ubicacion.getDerecha());
                }
            } else {
                throw new ArbolNException("Este Visitante Ya Calificó La Atracción");
            }

            ubicacion.actualizarAltura();
            balancear(ubicacion);
        }
    }

//    public ArrayList inOrden(NodoAVLEnc raiza) {
//        ArrayList listado = new ArrayList();
//        inOrden(raiza, listado);
//        return listado;
//    }
//
//    public void inOrden(NodoAVLEnc reco, ArrayList listado) {
//        if (reco != null) {
//            inOrden(reco.getIzquierda(), listado);
//            listado.add(reco.getDato() + " ");
//            inOrden(reco.getDerecha(), listado);
//        }
//    }

    public NodoAVL balancear(NodoAVL principal) throws ArbolNException {
        if (!principal.esVacio()) {
            int facEqu = principal.obtenerFactorEquilibrio();
            boolean signo = true;
            if (facEqu < 0) {
                signo = false;
                facEqu = facEqu * (-1);
            }
            if (facEqu >= 2) {
                //esta desequilibrado
                if (signo) {
                    //desequilibrado validar zig zag
                    if (principal.getDerecha().obtenerFactorEquilibrio() < 0) {
                        //rotacion doble
                        rotarSimpleNuevo(principal.getDerecha(), false);
                        rotarSimpleNuevo(principal, true);
                    } else {
                        rotarSimpleNuevo(principal, signo);
                    }
                } else {
                    if (principal.getIzquierda().obtenerFactorEquilibrio() > 0) {
                        rotarSimpleNuevo(principal.getIzquierda(), true);
                        rotarSimpleNuevo(principal, false);
                    } else {
                        rotarSimpleNuevo(principal, signo);
                    }
                }
            }
        }
        return principal;
    }

public void rotarSimpleNuevo(NodoAVL princ, boolean sentido) {

        if (!sentido) {
             if (princ.getDerecha()!= null) {
                NodoAVL nodo = princ.getDerecha();
                princ.setDerecha(new NodoAVL(princ.getDato()));
                princ.getDerecha().setDerecha(nodo);
            } else {
                princ.setDerecha(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getIzquierda().getDato());
            if (princ.getIzquierda().getDerecha()!= null) {
                NodoAVL nodo = princ.getIzquierda().getDerecha();
                 princ.setIzquierda(princ.getIzquierda().getIzquierda());
                 princ.getIzquierda().setDerecha(nodo);
            } else {
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
            }            
            princ.getDerecha().actualizarAltura();
        } else {
            if (princ.getIzquierda() != null) {
                NodoAVL nodo = princ.getIzquierda();
                princ.setIzquierda(new NodoAVL(princ.getDato()));
                princ.getIzquierda().setIzquierda(nodo);
            } else {
                princ.setIzquierda(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getDerecha().getDato());
            if (princ.getDerecha().getIzquierda() != null) {
                NodoAVL nodo = princ.getDerecha().getIzquierda();
                princ.setDerecha(princ.getDerecha().getDerecha());
                princ.getDerecha().setIzquierda(nodo);
            } else {
                princ.setDerecha(princ.getDerecha().getDerecha());
            }            
            princ.getIzquierda().actualizarAltura();
        }
        princ.actualizarAltura();
    }

}

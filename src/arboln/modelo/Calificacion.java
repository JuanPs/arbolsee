/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboln.modelo;

import avl.modelo.NodoAVL;
import java.util.Date;

/**
 *
 * @author juand
 */
public class Calificacion {
    
    
    private String descripcion;
    private String codigo;
    private boolean tipoCalificacion;
    private  int peso;
    private boolean estado;
    private Date fecha;
    public Calificacion() {
    }
    
    
    

    public Calificacion(String descripcion, String codigo, boolean tipoCalificacion, int peso, boolean estado) {
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.tipoCalificacion = tipoCalificacion;
        this.peso = peso;
        this.estado = estado;
    
    }

    public Calificacion(String descripcion, String codigo, boolean tipoCalificacion, int peso, boolean estado, Date fecha) {
        this.descripcion = descripcion;
        this.codigo = codigo;
        this.tipoCalificacion = tipoCalificacion;
        this.peso = peso;
        this.estado = estado;
        this.fecha = fecha;
    }
    

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isTipoCalificacion() {
        return tipoCalificacion;
    }

    public void setTipoCalificacion(boolean tipoCalificacion) {
        this.tipoCalificacion = tipoCalificacion;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

  
    
    @Override
    public String toString() {
        return descripcion;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboln.modelo;

import arbolse.modelo.excepciones.ArbolNException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aula205c
 */
public class ArbolN {
    private NodoN raiz;
    private String grupo;

    public ArbolN(String grupo) {
        this.grupo = grupo;
    }

    

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public ArbolN() {
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }
    
  
    public void insertarHijo(NodoN pivote, Visitante hijo, Visitante padre) throws ArbolNException {
        if (raiz == null) {
            raiz = new NodoN(hijo);
        } else {
            if (pivote.getDato().getIdentificacion().equals(padre.getIdentificacion())) {
                pivote.aumentarHijo(new NodoN(hijo));
            } else {
                for (NodoN hijoPivote : pivote.getHijos()) {
                    if (hijoPivote.getDato().getIdentificacion().equals(padre.getIdentificacion())) {
                        hijoPivote.aumentarHijo(new NodoN(hijo));
                        break;
                    } else {
                        insertarHijo(hijoPivote, hijo, padre);
                    }
                }
            }
        }
    }

    public String recorrerArbolN() throws ArbolNException {
        if (raiz != null) {
            String listado = "";
            listado += recorrerArbolN(raiz, listado);
        }
        throw new ArbolNException("EL Arbol Está Vacío");
    }

    public String recorrerArbolN(NodoN pivote, String listado) {
        listado += "\n" + pivote.getDato();
        for (NodoN hijo : pivote.getHijos()) {
            listado += recorrerArbolN(hijo, listado);
        }
        return listado;
    }

    Visitante papa = new Visitante();

    public Visitante buscarPadre(NodoN reco, String padre) throws ArbolNException {
        if (reco.getDato().getIdentificacion().equals(padre)) {
            return papa = reco.getDato();
        } else {
            for (NodoN hijo : reco.getHijos()) {
                papa = buscarPadre(hijo, padre);
            }
            return papa;
        }
    }

    public void isVisitanteRegistrado(String id, ArrayList<Visitante> listado) throws ArbolNException {
        for (int i = 0; i < listado.size(); i++) {
            if (listado.get(i).getIdentificacion().equals(id)) {
                throw new ArbolNException("Id Ya Fue Registrado");
            }
        }
    }
    private NodoN dato;

    public NodoN getDato() {
        return dato;
    }

    public void setDato(NodoN dato) {
        this.dato = dato;
    }
    
    public NodoN retornarDatosVisitanteNodo(String id, NodoN reco) throws ArbolNException {
        if (id.equals(reco.getDato().getIdentificacion())) {
            return dato=reco;
        } else {
            for (NodoN hijo : reco.getHijos()) {
                dato=retornarDatosVisitanteNodo(id, hijo);
            }
            return dato;
        }
    }

}

       

